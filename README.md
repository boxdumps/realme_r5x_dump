## raven-user 12 SD1A.210817.036 7805805 release-keys
- Manufacturer: realme
- Platform: trinket
- Codename: r5x
- Brand: Realme
- Flavor: nad_r5x-userdebug
- Release Version: 12
- Id: SQ1A.211205.008
- Incremental: eng.om.20220102.124447
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SD1A.210817.036/7805805:user/release-keys
- OTA version: 
- Branch: raven-user-12-SD1A.210817.036-7805805-release-keys
- Repo: realme_r5x_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
